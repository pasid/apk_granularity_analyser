*********APK GRANULARITY ANALYSER  *********


# GOAL
The APK Granularity Analyser Java project aims to provide simple statistics related to static code analysis of mobile applications.

# HOW IT WORKS?
-> The Granularity Analyser loads an .apk file
-> It transforms the .apk into a .jar file
-> The .jar file is uncompressed 
-> All the classes are recursively analyzed 
-> A report named reportComponent.txt is generated inside the directory generated_reports

# HOW TO USE IT?
You must to follow the three following steps:
-> You must to put your .apk inside the apks directory
-> You must to open the class analyser.main.Main.java and include the path+name of your .apk: Main.analyzeAPK("apks/YourAPKName.apk"); 
-> Runs the Main.java

# WHAT THE REPORT SHOWS?
1 - The report shows the bundles of classes. A bundle is a set of coupled classes in a tree data structure.
2 - Summary of Source Code Evaluation, including:
	a) Total Number of Classes
	b) Total Number of Methods
	c) Total Depth of Inheritance: The depth of inheritance (DI) metric provides for each class a measure of the inheritance levels from the object hierarchy top. In Java where all classes inherit Object the minimum value of DIT is 1.
	d) Total Coupling: The coupling between object classes (CBO) metric represents the number of classes coupled to a given class
	
3 - For each class:
	a) - The Number of Methods
	b) - Depth of Inheritance
	c) - Coupling between object classes
	