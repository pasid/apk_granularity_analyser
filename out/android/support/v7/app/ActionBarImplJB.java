package android.support.v7.app;

import android.app.*;
import android.view.*;
import android.content.*;
import android.graphics.drawable.*;
import android.widget.*;

public class ActionBarImplJB extends ActionBarImplICS
{
    public ActionBarImplJB(final Activity activity, final Callback callback) {
        super(activity, callback, false);
    }
}
