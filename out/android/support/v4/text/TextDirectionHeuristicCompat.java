package android.support.v4.text;

public interface TextDirectionHeuristicCompat
{
    boolean isRtl(CharSequence p0, int p1, int p2);
    
    boolean isRtl(char[] p0, int p1, int p2);
}
